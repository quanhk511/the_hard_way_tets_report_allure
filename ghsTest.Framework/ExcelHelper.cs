﻿using ExcelDataReader;
using System.Data;
using System.IO;
using System.Text;

namespace ghsTest.Framework
{
    public static class ExcelHelper
    {
        public static DataSet ConvertExcelToDataTable(string filePath, bool isXlsx = false)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //open file and returns as Stream
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {

                    var conf = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true
                        }
                    };

                    var dataSet = reader.AsDataSet(conf);

                    // Now you can get data from each sheet by its index or its "name"
                    //var dataTable = dataSet.Tables[0];

                    return dataSet;
                }
            }
        }

    }
}
