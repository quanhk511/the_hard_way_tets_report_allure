﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ghsTest.Framework
{
    public class Driver
    {
        public enum BrowserType
        {
            Chrome,
            Firefox,
            InternetExplorer
        }

        private static Driver instanceOfBrowser = null;
        public IWebDriver Browser;
        public WebDriverWait BrowserWait;

        private Driver(BrowserType browserType = BrowserType.Chrome, int defaultTimeOut = 30)
        {
            switch (browserType)
            {
                case BrowserType.Chrome:
                    Browser = new ChromeDriver(Path.Combine(Directory.GetCurrentDirectory(), "webdriver"));
                    break;
                case BrowserType.Firefox:
                    Browser = new FirefoxDriver(Path.Combine(Directory.GetCurrentDirectory(), "webdriver"));
                    break;
                case BrowserType.InternetExplorer:
                    break;
                default:
                    throw new ArgumentException("You need to set a support browser type.");
            }
            BrowserWait = new WebDriverWait(Browser, TimeSpan.FromSeconds(defaultTimeOut));
        }
        public static Driver getInstance(BrowserType browserType = BrowserType.Chrome, int defaultTimeOut = 30)
        {
            if (instanceOfBrowser == null)
            {
                instanceOfBrowser = new Driver(browserType, defaultTimeOut);
            }
            return instanceOfBrowser;
        }
    }
}
