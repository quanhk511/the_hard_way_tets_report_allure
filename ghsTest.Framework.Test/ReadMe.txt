﻿1. Install PowerShell 5 (or later) and .NET Framework 4.5 (or later). 
2. Installs Scoop via Powershell (https://scoop.sh/)
	Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
3. Install Allure Framework (https://docs.qameta.io/allure/#_installing_a_commandline)
4. install allure 
	scoop install allure
	allure serve D:\Projects\ghsTest\ghsTest.Framework.Test\bin\Debug\netcoreapp3.1\allure-results
	allure generate D:\Projects\ezLearnIt\ghsTest\ghsTest.Framework.Test\bin\Debug\netcoreapp3.1\allure-results --clean
