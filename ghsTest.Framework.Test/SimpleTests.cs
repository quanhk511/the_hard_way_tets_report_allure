using Allure.Commons;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.IO;

namespace ghsTest.Framework.Test
{
    [TestFixture]
    [AllureNUnit]
    [AllureDisplayIgnored]
    public class SimpleTests
    {
        private DataSet ds;
        IWebDriver driver;

        [SetUp]
        public void Setup()
        {
             driver = new ChromeDriver("/var/jenkins_home/workspace/allure_test_report/ghsTest.Framework.Test");
          
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), "Data/TestData.xlsx");
            ds = ExcelHelper.ConvertExcelToDataTable(filePath, true);
        }

        [TearDown]
        public void Dispose()
        {
            
            driver.Quit();
        }
                
        [Test(Description = "XXX")]
        [AllureTag("Regression")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureIssue("ISSUE-1")]
        [AllureTms("TMS-12")]
        [AllureOwner("User")]
        [AllureSuite("SimpleTest")]
        [AllureSubSuite("Assert")]
        public void Test1()
        {
            var dt = ds.Tables["Courses"];
            foreach (DataRow row in dt.Rows)
            {
                driver.Navigate().GoToUrl("https://google.com");
                 var wait = new WebDriverWait(driver, TimeSpan.FromMinutes(15));
                wait.Until(driver => driver.FindElement(By.Name("q")));
                IWebElement element = driver.FindElement(By.Name("q"));
                element.SendKeys(row["Name"].ToString());
                element.Submit();                
                System.Threading.Thread.Sleep(3000);
            }
            System.Threading.Thread.Sleep(5000);
            Assert.Fail();
        }
    }
}