﻿using System;
using System.Windows;
using MahApps.Metro.Controls;

namespace ghsTest.Studio
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }               

        private void OpenWebsite(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://ezlearnit.net/products/");
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            switch (this.WindowState)
            {
                case WindowState.Maximized:
                    break;
                case WindowState.Minimized:
                    // Do your stuff
                    break;
                case WindowState.Normal:

                    break;
            }
        }
    }
}
